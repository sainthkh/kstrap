<?php
/**
 * Understrap functions and definitions
 *
 * @package kstrap
 */

/**
 * Theme setup and custom theme supports.
 */
require get_template_directory() . '/inc/setup.php';

/**
 * Load util functions.
 */
require get_template_directory() . '/inc/util.php';

/**
 * Enqueue scripts and styles.
 */
require get_template_directory() . '/inc/enqueue.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/theme/setup.php';

/**
 * Use Blade template library.
 */
require get_template_directory() . '/inc/view.php';

require get_template_directory().'/inc/menu.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Load Editor functions.
 */
require get_template_directory() . '/inc/editor.php';
