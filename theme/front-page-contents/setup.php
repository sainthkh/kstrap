<?php 
add_action( 'admin_menu', 'front_page_contents_admin_menu' );

function front_page_contents_admin_menu() {
	add_menu_page( 'Front Page Contents', 'Front Page Contents', 'manage_options', 'kstrap/front-page-contents.php', 'front_page_contents_page', 'dashicons-admin-home', 6  );
}

function front_page_contents_page() {
	include_view('admin.fpc');
}