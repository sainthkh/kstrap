<?php
/**
 * Understrap Theme Customizer
 *
 * @package kstrap
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
if ( ! function_exists( 'kstrap_customize_register' ) ) {
	/**
	 * Register basic customizer support.
	 *
	 * @param object $wp_customize Customizer reference.
	 */
	function kstrap_customize_register( $wp_customize ) {
		$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
		$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
		$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
	}
}
add_action( 'customize_register', 'kstrap_customize_register' );

if ( ! function_exists( 'kstrap_theme_customize_register' ) ) {
	/**
	 * Register individual settings through customizer's API.
	 *
	 * @param WP_Customize_Manager $wp_customize Customizer reference.
	 */
	function kstrap_theme_customize_register( $wp_customize ) {
		require_once(get_template_directory() . '/theme/customizer/layout.php');
		require_once(get_template_directory() . '/theme/customizer/hero.php');
		require_once(get_template_directory() . '/theme/customizer/footer.php');
	}
} // endif function_exists( 'kstrap_theme_customize_register' ).
add_action( 'customize_register', 'kstrap_theme_customize_register' );

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
if ( ! function_exists( 'kstrap_customize_preview_js' ) ) {
	/**
	 * Setup JS integration for live previewing.
	 */
	function kstrap_customize_preview_js() {
		wp_enqueue_script( 'kstrap_customizer', get_template_directory_uri() . '/public/js/customizer.js',
			array( 'customize-preview' ), '20130508', true );
	}
}
add_action( 'customize_preview_init', 'kstrap_customize_preview_js' );

function kstrap_customization_setup() {
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'kstrap' ),
		'social' => __( 'Social Menu', 'kstrap' ),
		'legal' => __('Legal Menu', 'kstrap' ),
	) );

	set_theme_default( 'hero_headline', 'Learn how to speak Korean like native speakers');
	set_theme_default( 'hero_subhead', 'Sign up and get my exclusive tips about Korean language' );
	set_theme_default( 'hero_background_image', get_template_directory_uri() . '/public/img/header.jpg' );
	set_theme_default( 'footer_copyright', '©2017 WiseInit.com All rights reserved' );
}
add_action( 'after_setup_theme', 'kstrap_customization_setup' );