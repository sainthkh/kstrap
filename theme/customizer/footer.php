<?php

$wp_customize->add_section( 'footer_area', array(
	'title'          => __( 'Footer Area', 'kstrap' ),
	'capability'  => 'edit_theme_options',
	'description'    => __( 'You can change "about us", your contact information, opening hours here.' ),
	'theme_supports' => '',
	'priority'       => 190,
) );


// Settings

$wp_customize->add_setting( 'footer_cta_message', array(
	'transport' => 'postMessage',
) );

$wp_customize->add_setting( 'footer_cta_button_text', array(
	'transport' => 'postMessage',
) );

$wp_customize->add_setting( 'footer_cta_button_link', array(
	'transport' => 'postMessage',
) );

$wp_customize->add_setting( 'about_us', array(
) );

$wp_customize->add_setting( 'contact_business_address', array(
	'transport' => 'postMessage',
) );

$wp_customize->add_setting( 'contact_email', array(
	'transport' => 'postMessage',
) );

$wp_customize->add_setting( 'contact_phone_number', array(
	'transport' => 'postMessage',
) );

$wp_customize->add_setting( 'contact_fax_number', array(
	'transport' => 'postMessage',
) );

$wp_customize->add_setting( 'opening_hours', array(
) );

$wp_customize->add_setting( 'footer_copyright', array(
	'transport' => 'postMessage',
) );


// Controls

$wp_customize->add_control( 'footer_cta_message', array(
	'type' => 'text',
	'section' => 'footer_area',
	'label' => __( 'Footer Call to Action', 'kstrap' ),
	'description' => __( 'What do you want your visitors to do?' ),
) );

$wp_customize->add_control( 'footer_cta_button_text', array(
	'type' => 'text',
	'section' => 'footer_area',
	'description' => __( 'What are the words that summarize that action?' ),
) );

$wp_customize->add_control( 'footer_cta_button_link', array(
	'type' => 'text',
	'section' => 'footer_area',
	'description' => __( 'Which page will your visitors visit?' ),
) );

$wp_customize->add_control( 'about_us', array(
	'type' => 'textarea', 
	'section' => 'footer_area',
	'label' => __( 'About Us', 'kstrap' ),
	'description' => __( 'The summary of your business: Who do you serve? What can you do for them? What is the benefit?' ),
) );

$wp_customize->add_control( 'contact_business_address', array(
	'type' => 'text',
	'section' => 'footer_area',
	'label' => __( 'Contact Information', 'kstrap' ),
	'description' => __( 'If your visitors want to learn more about you, where should they contact?', 'kstrap' ),
	'input_attrs' => array(
		'placeholder' => __( 'address', 'kstrap' ),
	),
) );

$wp_customize->add_control( 'contact_email', array(
	'type' => 'text',
	'section' => 'footer_area',
	'input_attrs' => array(
		'placeholder' => __( 'email address', 'kstrap' ),
	),
) );

$wp_customize->add_control( 'contact_phone_number', array(
	'type' => 'text',
	'section' => 'footer_area',
	'input_attrs' => array(
		'placeholder' => __( 'phone number', 'kstrap' ),
	),
) );

$wp_customize->add_control( 'contact_fax_number', array(
	'type' => 'text',
	'section' => 'footer_area',
	'input_attrs' => array(
		'placeholder' => __( 'fax number', 'kstrap' ),
	),
) );

$wp_customize->add_control( 'opening_hours', array(
	'type' => 'textarea', 
	'section' => 'footer_area',
	'label' => __( 'Opening Hours', 'kstrap' ),
	'description' => __( 'When do you open?' ),
) );

$wp_customize->add_control( 'footer_copyright', array(
	'type' => 'text',
	'section' => 'footer_area',
	'label' => __( 'Copyright Text', 'kstrap' ),
	'description' => __( 'State the copyright information about this website', 'kstrap' ),
) );