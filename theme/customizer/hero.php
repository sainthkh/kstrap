<?php

$wp_customize->add_section( 'hero_area', array(
	'title'          => __( 'Hero Area' ),
	'capability'  => 'edit_theme_options',
	'description'    => __( 'You can change headline, subhead, call to action button and background image of hero area here.' ),
	'theme_supports' => '',
	'priority'       => 60,
) );

$wp_customize->add_setting( 'hero_headline', array(
	'transport'         => 'postMessage',
) );

$wp_customize->add_setting( 'hero_subhead', array(
	'transport'			=> 'postMessage',
) );

$wp_customize->add_setting( 'hero_cta_text', array(
	'transport'			=> 'postMessage',
) );

$wp_customize->add_setting( 'hero_cta_link', array(
	'transport'			=> 'postMessage',
) );

$wp_customize->add_setting( 'signup_form_link', array(
	'transport'			=> 'postMessage',
) );


$wp_customize->add_setting( 'hero_background_image', array(
) );

$wp_customize->add_control( 'hero_headline', array(
	'type' => 'textarea',
	'section' => 'hero_area',
	'label' => __( 'Headline', 'kstrap' ),
	'description' => __( 'The headline of your website. It should give an irresistible offer that your visitors will love.' ),
) );

$wp_customize->add_control( 'hero_subhead', array(
	'type' => 'textarea',
	'section' => 'hero_area',
	'label' => __( 'Subhead', 'kstrap' ),
	'description' => __( 'The subhead of your website. You can make this empty.' ),
) );

$wp_customize->add_control( 'hero_cta_text', array(
	'type' => 'text',
	'section' => 'hero_area',
	'label' => __( 'Call to Action Button', 'kstrap' ),
	'description' => __( 'What do you want your visitors to do?' ),
) );

$wp_customize->add_control( 'hero_cta_link', array(
	'type' => 'text',
	'section' => 'hero_area',
	'description' => __( 'Which page will your visitors visit?' ),
) );

$wp_customize->add_control( 'signup_form_link', array(
	'type' => 'text',
	'section' => 'hero_area',
	'label' => __( 'Sign up Form Link', 'kstrap' ),
) );

$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'hero_background_image', array(
	'label' => __( 'Background Image', 'kstrap' ),
	'description' => __( 'You can change the background image of your hero area.', 'kstrap' ),
	'settings' => 'hero_background_image',
	'section' => 'hero_area',
) ) );