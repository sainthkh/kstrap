<?php

$wp_customize->add_section( 'kstrap_theme_layout_options', array(
	'title'       => __( 'Theme Layout Settings', 'kstrap' ),
	'capability'  => 'edit_theme_options',
	'description' => __( 'Container width and sidebar defaults', 'kstrap' ),
	'priority'    => 160,
) );

	//select sanitization function
function kstrap_theme_slug_sanitize_select( $input, $setting ){
	
	//input must be a slug: lowercase alphanumeric characters, dashes and underscores are allowed only
	$input = sanitize_key($input);

	//get the list of possible select options 
	$choices = $setting->manager->get_control( $setting->id )->choices;
						
	//return input if valid or return default option
	return ( array_key_exists( $input, $choices ) ? $input : $setting->default );                
		
}

$wp_customize->add_setting( 'kstrap_sidebar_active', array(
	'default'           => 'false',
	'type'              => 'theme_mod',
	'sanitize_callback' => 'sanitize_text_field',
	'capability'        => 'edit_theme_options',
) );

$wp_customize->add_control(
	new WP_Customize_Control(
		$wp_customize,
		'kstrap_sidebar_active', array(
			'label'       => __( 'Activate Sidebar', 'kstrap' ),
			'description' => __( "If you set it to active, sidebar will appear on the right side of the page. <br> Note: You can override this setting by using 'Full width' or 'Empty' page template.", 'kstrap' ),
			'section'     => 'kstrap_theme_layout_options',
			'settings'    => 'kstrap_sidebar_active',
			'type'        => 'select',
			'sanitize_callback' => 'kstrap_theme_slug_sanitize_select',
			'choices'     => array(
				'active' => __( 'Activate', 'kstrap' ),
				'false'  => __( 'No sidebar', 'kstrap' ),
			),
			'priority'    => '20',
		)
	) 
);