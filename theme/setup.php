<?php
/**
 * Setup theme. 
 *
 * @package kstrap
 */

require_once get_template_directory().'/theme/customizer/setup.php';
require_once get_template_directory().'/theme/front-page-contents/setup.php';

// Set the content width based on the theme's design and stylesheet.
if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}


