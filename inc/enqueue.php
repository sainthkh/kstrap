<?php
/**
 * Understrap enqueue scripts
 *
 * @package kstrap
 */

if ( ! function_exists( 'kstrap_scripts' ) ) {
	/**
	 * Load theme's JavaScript sources.
	 */
	function kstrap_scripts() {
		// Get the theme data.
		$the_theme = wp_get_theme();
		wp_enqueue_style( 'kstrap-styles', get_stylesheet_directory_uri() . '/public/css/theme.min.css', array(), $the_theme->get( 'Version' ), false );
		wp_enqueue_script( 'understrap-scripts', get_template_directory_uri() . '/public/js/theme.min.js', array(), $the_theme->get( 'Version' ), true );
		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}
	}
} // endif function_exists( 'kstrap_scripts' ).

add_action( 'wp_enqueue_scripts', 'kstrap_scripts' );

if ( ! function_exists( 'kstrap_admin_scripts' ) ) {
	/**
	 * Load theme's JavaScript sources.
	 */
	function kstrap_admin_scripts() {
		wp_enqueue_style( 'kstrap-admin-styles', get_stylesheet_directory_uri() . '/public/css/admin.min.css');
		wp_enqueue_script( 'admin-js', get_template_directory_uri() . '/public/js/admin.js' );
		wp_enqueue_script( 'fontawesome', 'https://use.fontawesome.com/releases/v5.0.7/js/all.js');
	}
} 

add_action( 'admin_enqueue_scripts', 'kstrap_admin_scripts' );