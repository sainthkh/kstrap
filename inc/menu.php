<?php

function kstrap_menu_items($theme_location) {
	if ( ! has_nav_menu($theme_location) ) {
		return array();
	}

	$menu = wp_get_nav_menu_object( get_nav_menu_locations()[$theme_location] );
    $menu_items = wp_get_nav_menu_items( $menu->term_id, array( 'update_post_term_cache' => false ) );
	$menu_data = array();
	
    foreach( $menu_items as $menu_item ) {
        if( $menu_item->menu_item_parent == 0 ) {
            $menu_data[$menu_item->ID] = array(
                'url' => $menu_item->url,
                'name' => $menu_item->title,
                'active' => $menu_item->url == kstrap_current_url(),
                'children' => array(),
            );
        }
    }

    foreach( $menu_items as $menu_item ) {        
        if ( $menu_item->menu_item_parent != 0 && isset( $menu_data[$menu_item->menu_item_parent] ) ) {
            $menu_data[$menu_item->menu_item_parent]['children'][$menu_item->ID] = array(
                'url' => $menu_item->url,
                'name' => $menu_item->title,
                'active' => $menu_item->url == kstrap_current_url(),
            );

            if ( $menu_item->url == kstrap_current_url() ) {
                $menu_data[$menu_item->menu_item_parent]['active'] = true;
            }
        }
	}
	
	return $menu_data;
}

function kstrap_social_icon_class($url) {
	$social_icons = array(
		'amazon.com'      => 'fab fa-amazon',
		'behance.net'     => 'fab fa-behance',
		'codepen.io'      => 'fab fa-codepen',
		'deviantart.com'  => 'fab fa-deviantart',
		'digg.com'        => 'fab fa-digg',
		'docker.com'      => 'fab fa-docker',
		'dribbble.com'    => 'fab fa-dribbble',
		'dropbox.com'     => 'fab fa-dropbox',
		'facebook.com'    => 'fab fa-facebook',
		'flickr.com'      => 'fab fa-flickr',
		'foursquare.com'  => 'fab fa-foursquare',
		'getpocket.com'   => 'fab fa-get-pocket',
		'github.com'      => 'fab fa-github',
		'gitlab.com'      => 'fab fa-gitlab',
		'instagram.com'   => 'fab fa-instagram',
		'itunes.com'      => 'fab fa-itunes',
		'kickstarter.com' => 'fab fa-kickstarter',
		'line.com'        => 'fab fa-line',
		'linkedin.com'    => 'fab fa-linkedin',
		'mailto:'         => 'fas envelope-o',
		'medium.com'      => 'fab fa-medium',
		'patreon.com'     => 'fab fa-patreon',
		'pinterest.com'   => 'fab fa-pinterest-p',
		'plus.google.com' => 'fab fa-google-plus',
		'podbean.com'     => 'fas fa-microphone',
		'pscp.tv'         => 'fab fa-periscope',
		'quora.com'       => 'fab fa-quora',
		'reddit.com'      => 'fab fa-reddit-alien',
		'scribd.com'      => 'fab fa-scribd',
		'slack.com'       => 'fab fa-slack',
		'skype.com'       => 'fab fa-skype',
		'skype:'          => 'fab fa-skype',
		'slideshare.net'  => 'fab fa-slideshare',
		'snapchat.com'    => 'fab fa-snapchat-ghost',
		'soundcloud.com'  => 'fab fa-soundcloud',
		'spotify.com'     => 'fab fa-spotify',
		'stackexchange.com' => 'fab fa-stack-exchange',
		'stackoverflow.com' => 'fab fa-stack-overflow',
		'steampowered.com' => 'fab fa-steam',
		'stumbleupon.com' => 'fab fa-stumbleupon',
		'tumblr.com'      => 'fab fa-tumblr',
		'twitch.tv'       => 'fab fa-twitch',
		'twitter.com'     => 'fab fa-twitter',
		'vimeo.com'       => 'fab fa-vimeo',
		'vine.co'         => 'fab fa-vine',
		'vk.com'          => 'fab fa-vk',
		'weibo.com'       => 'fab fa-weibo',
		'whatsapp.com'    => 'fab fa-whatsapp.com',
		'wordpress.org'   => 'fab fa-wordpress',
		'wordpress.com'   => 'fab fa-wordpress',
		'yelp.com'        => 'fab fa-yelp',
		'youtube.com'     => 'fab fa-youtube',
	);

	foreach ( $social_icons as $attr => $value ) {
		if ( false !== strpos( $url, $attr ) ) {
			return $value;
		}
	}

	return 'fas fa-link';
}