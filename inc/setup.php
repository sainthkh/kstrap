<?php
/**
 * Theme setup that doesn't change from theme to theme.
 * The theme specific setup is in /setting/cutomizer/setup.php
 *
 * @package kstrap
 */

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function kstrap_basic_setup() {
	/*
		* Make theme available for translation.
		* Translations can be filed in the /languages/ directory.
		* If you're building a theme based on understrap, use a find and replace
		* to change 'kstrap' to the name of your theme in all the template files
		*/
	load_theme_textdomain( 'kstrap', get_template_directory() . '/resources/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
		* Let WordPress manage the document title.
		* By adding theme support, we declare that this theme does not use a
		* hard-coded <title> tag in the document head, and expect WordPress to
		* provide it for us.
		*/
	add_theme_support( 'title-tag' );

	/*
		* Switch default core markup for search form, comment form, and comments
		* to output valid HTML5.
		*/
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
		* Adding Thumbnail basic support
		*/
	add_theme_support( 'post-thumbnails' );

	/*
		* Adding support for Widget edit icons in customizer
		*/
	add_theme_support( 'customize-selective-refresh-widgets' );

	/*
		* Enable support for Post Formats.
		* See http://codex.wordpress.org/Post_Formats
		*/
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress Theme logo feature.
	add_theme_support( 'custom-logo' );

	register_sidebar( array(
		'name'          => __( 'Sidebar', 'kstrap' ),
		'id'            => 'sidebar',
		'description'   => 'Sidebar widget area',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
}
add_action( 'after_setup_theme', 'kstrap_basic_setup' );

function set_theme_default( $name, $value ) {
	// check if settings are set, if not set defaults.
	// Caution: DO NOT check existence using === always check with == .
	$current = get_theme_mod( $name );
	if ( '' === $current ) {
		set_theme_mod( $name, $value );
	}
}

// Remove [...] at the end of post summary.
function kstrap_custom_excerpt_more( $more ) {
	return '';
}
add_filter( 'excerpt_more', 'kstrap_custom_excerpt_more' );

function filter_rest_allow_anonymous_comments() {
    return true;
}
add_filter('rest_allow_anonymous_comments','filter_rest_allow_anonymous_comments');

// Disable use XML-RPC
add_filter( 'xmlrpc_enabled', '__return_false' );

// Disable X-Pingback to header
add_filter( 'wp_headers', 'disable_x_pingback' );
function disable_x_pingback( $headers ) {
    unset( $headers['X-Pingback'] );

return $headers;
}

function kstrap_rest_count_comments( $data ) {
	return get_comments_number( $data['id'] );
}

add_action( 'rest_api_init', function () {
	register_rest_route( 'kstrap/v1', '/comments/count/(?P<id>\d+)', array(
		'methods' => 'GET',
		'callback' => 'kstrap_rest_count_comments',
	) );
} );

function kstrap_rest_all_comments( $data ) {
	$comments_query = new WP_Comment_Query;
	$data = $comments_query->query(array(
		'post_id' => $data['id'],
		'order' => 'ASC',
	));

	$comments = [];
	foreach($data as $raw) {
		$comments[] = array(
			'id'                 => (int) $raw->comment_ID,
			'parent'             => (int) $raw->comment_parent,
			'author_name'        => $raw->comment_author,
			'author_email'       => $raw->comment_author_email,
			'author_url'         => $raw->comment_author_url,
			'date'               => mysql_to_rfc3339( $raw->comment_date ),
			'content'            => array(
				/** This filter is documented in wp-includes/comment-template.php */
				'rendered' => apply_filters( 'comment_text', $raw->comment_content, $raw ),
				'raw'      => $raw->comment_content,
			),
			'author_avatar_urls' => rest_get_avatar_urls( $raw->comment_author_email ),
		);
	}

	return $comments;
}

add_action( 'rest_api_init', function () {
	register_rest_route( 'kstrap/v1', '/comments/all/(?P<id>\d+)', array(
		'methods' => 'GET',
		'callback' => 'kstrap_rest_all_comments',
	) );
} );

function kstrap_rest_can_edit_comments( $data ) {
	return current_user_can('moderate_comments') || current_user_can('edit_comment');
}

add_action( 'rest_api_init', function () {
	register_rest_route( 'kstrap/v1', '/comments/can-edit', array(
		'methods' => 'GET',
		'callback' => 'kstrap_rest_can_edit_comments',
	) );
} );
