@extends('layouts.bone')

@section('body')
<div class="hfeed site" id="page">
	@if ( kstrap_front_page() ) 
		@include('components.layout.header')
	@else
		@include('components.layout.navbar')
	@endif

	@yield('content')
	@include('components.layout.footer')
</div>
@endsection