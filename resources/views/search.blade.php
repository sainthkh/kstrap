@extends('layouts.default')

@section('content') 
<div class="container" id="content" tabindex="-1">
	<div class="row">
		<div class="col-md-8 offset-md-2 content-area" id="primary">
			<main class="site-main" id="main">
                <div class="container" id="content" tabindex="-1">
                    <div class="row">
                        <?php get_template_part( 'template-parts/anywhere/open-primary-tag' ); ?>
                            <main class="site-main" id="main">
                                <?php if ( have_posts() ) : ?>
                                    <header class="page-header mb-5">
                                        <h1 class="page-title">Search Results for "{{ get_search_query() }}"</h1>
                                    </header>
                                    <?php while ( have_posts() ) : the_post(); ?>
                                        @include('shared.excerpt')
                                    <?php endwhile; ?>
                                <?php else : ?>
                                    <p>No post found</p>
                                <?php endif; ?>
                            </main>
                            @include('components.pagination')
                        </div>
                    </div>
                </div>
			</main>
		</div>
	</div>
</div>
@endsection