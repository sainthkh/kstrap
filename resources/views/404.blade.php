@extends('layouts.default')

@section('content')
<div class="container" id="content" tabindex="-1">
	<div class="row">
		<div class="col-md-8 content-area" id="primary">
            <main class="site-main" id="main">
                <section class="error-404 not-found">
                    <header class="page-header">
                        <h1 class="page-title">Oops! That page can't be found!</h1>
                    </header>
                    <div class="page-content">
                        <p>It looks like nothing was found at this location. Maybe try one of the links below or a search?</p>
                    </div>
                </section>
            </main>
        </div>
    </div>
</div>
@endsection