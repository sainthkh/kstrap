@extends('layouts.default')

@section('content') 
<div class="main-container" id="content" tabindex="-1">
	<main class="site-main"	id="main">
		@include('shared.recommended-contents')
	</main>
</div>
@endsection