<form action="@mod('signup_form_link')" class="validate" target="_blank" novalidate>
	<div class="row">
		<div class="mc-field-group col-md-3">
			<input type="text" value="" name="FNAME" class="form-control" id="mce-FNAME" placeholder="your first name">
		</div>
		<div class="mc-field-group col-md-6">
			<input type="email" value="" name="EMAIL" class="form-control required email" id="mce-EMAIL" placeholder="your@email.com">
		</div>
		<div class="col-md-3">
			<input type="submit" value="Sign up" name="subscribe" id="mc-embedded-subscribe" class="btn btn-primary btn-block">
		</div>
	</div>
</form>