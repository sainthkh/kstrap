<footer class="site-footer" id="wrapper-footer">
	<div class="footer-cta">
		<div class="container">
			<div class="row">
				<div class="col-md-9 bottom-cta-message">
					@mod('footer_cta_message')
				</div>
				<div class="col-md-3 flex-vertical-center">
					<a class="btn btn-primary btn-lg" href="@mod('footer_cta_link')">@mod('footer_cta_button_text')</a>
				</div>
			</div>
		</div>
	</div>
	<div class="footer-main">
		<div class="container">
			<div class="row">
				<div class="col-md-4 footer-main-section">
					<div>
						<h4>About us</h4>
						<hr>
					</div>
					<div>
						@modp('about_us')
					</div>
					<div>
						@include('components.nav.social')
					</div>
				</div>
				<div class="col-md-4 footer-main-section">
					<div class="search-form-wrap">
						<h4>Search</h4>
						<hr>
						@include('components.searchform')
					</div>
					<div>
						<h4>Contact Info</h4>
						<hr>
						<ul>
							<li><i class="footer-icon fas fa-home"></i>New York, NY, 12345, US
							<li><i class="footer-icon fas fa-envelope"></i>great@gmail.com
							<li><i class="footer-icon fas fa-phone"></i>123 445 6677
							<li><i class="footer-icon fas fa-fax"></i>774 882 9991
						</ul>
					</div>
				</div>
				<div class="col-md-4 footer-main-section">
					<div>
						<h4>Opening hours</h4>
						<hr>
					</div>
					<div>
						@modp( 'opening_hours' )
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="footer-copyright">
		<div class="container">
			<div class="row">
				<div class="col-12">
					@mod('footer_copyright')
				</div>
			</div>
		</div>
	</div>
</footer>