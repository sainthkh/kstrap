<nav class="topnav">
    <div class="topnav-container">
        @unless(has_custom_logo()) 
            <a class="topnav-brand" rel="home" href="{{ home_url( '/' ) }}" title="{{ esc_attr( get_bloginfo( 'name', 'display' ) ) }}"><?php bloginfo( 'name' ); ?></a>
        @else 
            <?php the_custom_logo(); ?>
        @endunless
        
        <button class="topnav-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="topnav-toggler-icon fa fa-bars"></span>
        </button>
        <div class="topnav-collapse" id="navbarTogglerDemo02">
            @include('components.nav.main-menu', [ 
                'nav_menu' => 'primary',
                'nav_class_name' => 'topnav-main-nav',
            ])
            @include('components.nav.social', [ 
                'nav_class_name' => 'topnav-social-nav',
            ])
        </div>
    </div>
</nav>