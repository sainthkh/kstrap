<?php 
    $menu_items = kstrap_menu_items($nav_menu);
?>

<ul class="{{ $nav_class_name }}">
    <?php $loop_index = 0; ?>
    @foreach($menu_items as $menu_item) 
        @if(empty($menu_item['children']))
            <li class="nav-item @echoif('active', $menu_item['active'])">
                <a class="nav-link" href="{{ $menu_item['url'] }}">
                    {{ $menu_item['name'] }} 
                    @if($menu_item['active']) 
                        <span class="screen-reader-text">(current)</span>
                    @endif
                </a>
            </li>
        @else
            <li class="nav-item dropdown @echoif('active', $menu_item['active'])">
                <a class="nav-link dropdown-toggle" href="#" id="menu-{{$loop_index}}" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    {{ $menu_item['name'] }}
                </a>
                <div class="dropdown-menu" aria-labelledby="menu-{{$loop_index}}">
                    @foreach($menu_item['children'] as $child_item) 
                        @if(!preg_match('/^-+$/', $child_item['name']))
                            <a class="dropdown-item @echoif('active', $child_item['active'])" href="{{ $child_item['url'] }}">{{ $child_item['name'] }}</a>
                        @else 
                            <div class="dropdown-divider"></div>
                        @endif
                    @endforeach
                </div>
            </li>
        @endif
        <?php $loop_index++; ?>
    @endforeach
</ul>
