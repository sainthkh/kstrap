<?php
    $menu_items = kstrap_menu_items('social');
?>

<ul class="{{ $nav_class_name }}">
    @foreach($menu_items as $menu_item) 
        <li class="nav-item">
            <a class="nav-link" href="{{ $menu_item['url'] }}">
                <span class="screen-reader-text">{{ $menu_item['name'] }}</span>
                <i class="{{ kstrap_social_icon_class($menu_item['url']) }}"></i>
            </a>
        </li>
    @endforeach
</ul>