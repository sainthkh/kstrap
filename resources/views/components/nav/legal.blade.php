<?php
    $menu_items = kstrap_menu_items('legal');
?>

<ul class="nav">
    @foreach($menu_items as $menu_item) 
        <li class="nav-item">
            <a class="nav-link" href="{{ $menu_item['url'] }}">
                {{ $menu_item['name'] }} 
            </a>
        </li>
    @endforeach
</ul>