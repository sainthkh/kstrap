<article {{ post_class('mb-5') }} id="post-@id">
	<header class="entry-header">
        <h2 class="entry-title">
            <a href="@permalink" rel="bookmark">@title</a>
        </h2>
    </header>
    <div class="entry-thumbnail">
        {!! get_the_post_thumbnail( get_the_ID(), 'large', array('class' => 'mt-2 mb-4' ) ) !!}
    </div>
	<div class="entry-content">
        {{ the_excerpt() }}
        <p><a class="btn btn-read-more understrap-read-more-link" href="@permalink">@wptext('Read More...')</a></p>
	</div>
	<hr>
</article>