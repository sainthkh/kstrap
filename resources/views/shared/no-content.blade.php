<section class="no-results not-found">
    <header class="page-header">
        <h1 class="page-title">
            {{ esc_html_e( 'Nothing Found', 'kstrap' ) }}
        </h1>
    </header>
    <div class="page-content">
        @if( is_home() && current_user_can( 'publish_posts' ) )
            <p>{{ __( 'Ready to publish your first post?', 'kstrap' ) }}</p>
            <p><a href="{{ esc_url( admin_url( 'post-new.php' ) ) }}">{{ __( 'Get Started Here', 'kstrap' ) }}</a></p>
        @else 
            <p>{{ __( 'This blog is empty.' )}}</p>
        @endif
    </div>
</section>