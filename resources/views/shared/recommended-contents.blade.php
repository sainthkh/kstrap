<section class="fpc-section">
    <h2 class="fpc-section-title">Case Studies</h2>
    <hr class="fpc-hr" />
    <div class="fpc-article featured-overlay">
        <div class="fpc-article-thumbnail" style="background-image:url('https://images.unsplash.com/photo-1495754149474-e54c07932677')"></div>
        <div class="fpc-article-text">
            <h3 class="fpc-article-title"><a href="#">Learn how Sally saved $10,211.34 when she bought her first home</a></h3>
            <p class="fpc-article-summary">Simply put, Chris is amazing. Smart, attentive and responsive. He will find the right deal that you need.</p>
        </div>
    </div>
    <div class="fpc-article image-left-small">
        <div class="fpc-article-thumbnail" style="background-image:url('https://images.unsplash.com/photo-1502672023488-70e25813eb80')"></div>
        <div class="fpc-article-text">
            <h3 class="fpc-article-title"><a href="#">Rachel found her dream home. Here's the full story.</a></h3>
            <p class="fpc-article-summary">Chris understands what we're looking for. We're thrilled to be moving in next weekend.</p>
        </div>
    </div>
    <div class="fpc-article image-left-small">
        <div class="fpc-article-thumbnail" style="background-image:url('https://images.unsplash.com/photo-1508831770358-a759999c2f9c')"></div>
        <div class="fpc-article-text">
            <h3 class="fpc-article-title"><a href="#">Joey thought selling her home would be a nightmare. But it wasn't. Wanna know why?</a></h3>
            <p class="fpc-article-summary">Chris was patient and reassuring during some very tense months.</p>
        </div>
    </div>
    <div class="fpc-button-wrap">
        <a class="fpc-read-more-button" href="#">Read more case studies</a>
    </div>
</section>
<section class="fpc-section">
    <h2 class="fpc-section-title">Neighborhood information</h2>
    <hr class="fpc-hr" />
    <div class="fpc-article">
        <div class="fpc-article-thumbnail" style="background-image:url('https://images.unsplash.com/photo-1513003604845-103fe8aaf4e6')"></div>
        <div class="fpc-article-text">
            <h3 class="fpc-article-title"><a href="#">7 best place for lunch when you want to try something new.</a></h3>
            <p class="fpc-article-summary">Simply put, Chris is amazing. Smart, attentive and responsive. He will find the right deal that you need.</p>
        </div>
    </div>
    <div class="fpc-article">
        <div class="fpc-article-thumbnail" style="background-image:url('https://images.unsplash.com/photo-1464020486846-34aa429118c1')"></div>
        <div class="fpc-article-text">
            <h3 class="fpc-article-title"><a href="#">Do you love running? Here's the list of great parks.</a></h3>
            <p class="fpc-article-summary">Chris understands what we're looking for. We're thrilled to be moving in next weekend.</p>
        </div>
    </div>
    <div class="fpc-article">
        <div class="fpc-article-thumbnail" style="background-image:url('https://images.unsplash.com/photo-1502214722586-9c0a74759710')"></div>
        <div class="fpc-article-text">
            <h3 class="fpc-article-title"><a href="#">Exciting festivals throught the year in</a></h3>
            <p class="fpc-article-summary">Chris was patient and reassuring during some very tense months.</p>
        </div>
    </div>
    <div class="fpc-button-wrap">
        <a class="fpc-read-more-button" href="#">Learn more about the neighborhood</a>
    </div>
</section>
<div class="fpc-section">
    <h2 class="fpc-section-title">Tips</h2>
    <hr class="fpc-hr" />
    <div class="fpc-article">
        <div class="fpc-article-thumbnail" style="background-image:url('https://images.unsplash.com/photo-1518183214770-9cffbec72538')"></div>
        <div class="fpc-article-text">
            <h3 class="fpc-article-title"><a href="#">11 taxes to pay when you buy a home.</a></h3>
            <p class="fpc-article-summary">Simply put, Chris is amazing. Smart, attentive and responsive. He will find the right deal that you need.</p>
        </div>
    </div>
    <div class="fpc-article">
        <div class="fpc-article-thumbnail" style="background-image:url('https://images.unsplash.com/photo-1475855581690-80accde3ae2b')"></div>
        <div class="fpc-article-text">
            <h3 class="fpc-article-title"><a href="#">17 things you need to check to buy your dream home.</a></h3>
            <p class="fpc-article-summary">Chris understands what we're looking for. We're thrilled to be moving in next weekend.</p>
        </div>
    </div>
    <div class="fpc-article">
        <div class="fpc-article-thumbnail" style="background-image:url('https://images.unsplash.com/photo-1517097473408-c0d7983cb95c')"></div>
        <div class="fpc-article-text">
            <h3 class="fpc-article-title"><a href="#">3 things to do to sell your home in higher price.</a></h3>
            <p class="fpc-article-summary">Chris was patient and reassuring during some very tense months.</p>
        </div>
    </div>
    <div class="fpc-button-wrap">
        <a class="fpc-read-more-button" href="#">Read more tips</a>
    </div>
</div>
