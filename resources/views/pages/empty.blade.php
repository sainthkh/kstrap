<?php
/**
 * Template Name: Empty Page Template
 */
?>

@extends('layouts.bone')

@section('body')
	@wpposts
		{{ the_content() }}
	@endposts
@endsection
