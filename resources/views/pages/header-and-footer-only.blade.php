<?php
/**
 * Template Name: Header and Footer Only
 */
?>

@extends('layouts.default')

@section('content')
	@wpposts
		{{ the_content() }}
	@endposts
@endsection