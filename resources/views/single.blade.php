@extends('layouts.default')

@section('content')
<div class="container" id="content" tabindex="-1">
	<div class="row">
		<div class="col-md-8 offset-md-2 content-area" id="primary">
			<main class="site-main" id="main">
				<article {!! post_class() !!} id="post-@id">
					<header class="entry-header">
						<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
					</header>

					<?php echo get_the_post_thumbnail( $post->ID, 'large' ); ?>

					<div class="entry-content">
						@wpposts
							<?php the_content(); ?>
							<?php
								wp_link_pages( array(
									'before' => '<div class="page-links">' . __( 'Pages:', 'kstrap' ),
									'after'  => '</div>',
								) );
							?>
						@endposts
					</div>
				</article>
			</main>
		</div>
	</div>
</div>
@endsection