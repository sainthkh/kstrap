window.Vue = require('vue');

require('./components/dropdown');
require('./components/navbar');
require('./pages/single');

Vue.component('comment-list', require('./components/comment/list.vue'));
Vue.component('comment', require('./components/comment/comment.vue'));
Vue.component('comment-form', require('./components/comment/form.vue'));
Vue.component('edit-form', require('./components/comment/edit-form.vue'));

const app = new Vue({
    el: '#app'
});
