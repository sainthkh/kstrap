import $ from 'cash-dom';

$('.menu-button a').on('click', function(e) {
	$('body').toggleClass('hide-toc');
	
    localStorage.setItem("hideTOC", $('body').hasClass('hide-toc'));
    e.preventDefault();
})
