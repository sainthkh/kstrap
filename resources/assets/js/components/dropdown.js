import $ from 'cash-dom';

$('.dropdown-toggle').on('click', function(e){
    var id = $(this).attr('id');

    $(this).attr('aria-expanded', !$(this).attr('aria-expanded'));
    $(`[aria-labelledby="${id}"]`).toggleClass("show");
    
    e.stopPropagation();
    e.preventDefault();
});

$(document).on('click', function(e){
    $('.dropdown-menu').removeClass('show');
})