import axios from 'axios';

export default {
    __comments: {},
    __list: {},
    __canEdit: null,
    size: 24,

    init(rawComments, size = 24) {
        this.size = size;

        this.addBulk(rawComments);
    },

    addBulk(comments) {
        for(let c of comments) {
            this.add(c);
        }
    },

    add(raw, editable = false) {
        if (raw.parent == 0) {
            this.__comments[raw.id] = this.extractCommentInfo(raw, this.size, editable);
        } else {
            this.__comments[raw.parent].children.push(this.extractCommentInfo(raw, this.size, editable));
        }
    },

    edit(raw, editable) {
        if (raw.parent == 0) {
            this.__comments[raw.id] = this.extractCommentInfo(raw, this.size, editable);
        } else {
            var children = this.__comments[raw.parent].children;
            var index = children.findIndex(comment => {
                return comment.id == raw.id;
            })
            children[index] = (this.extractCommentInfo(raw, this.size, editable));
        }
    },

    extractCommentInfo(raw, size, editable) {
        return {
            id: raw.id,
            authorName: raw.author_name,
            authorUrl: raw.author_url,
            avatarUrl: raw.author_avatar_urls[size],
            content: raw.content,
            date: raw.date,
            children: [],
            editable,
        }
    },

    list() {
        Vue.set(this.__list, 'comments', Object.values(this.__comments))
    },

    async currentUserCanEditComment() {
        if (this.__canEdit == null) {
            var res = await axios.request({
                url: `${kstrapApiUrl}/comments/can-edit`,
                method: 'GET',
                headers: {
                    'X-WP-Nonce': restNonce,
                },
            })
            this.__canEdit = res.data;
        }
        
        return this.__canEdit;
    },
}