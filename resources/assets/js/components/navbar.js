import $ from 'cash-dom';

$('.topnav-toggler').on('click', function(e){
    $(this).attr('aria-expanded', !$(this).attr('aria-expanded'));

    $(`${$(this).data('target')}`).toggleClass("show");

    e.stopPropagation();
    e.preventDefault();
})

function useAddColor() {
    const OFFSET_TOP = 50;

    $(window).on('scroll', function() {
        if($('.topnav').offset().top > OFFSET_TOP) {
            $('.topnav').addClass('add-color');
        } else {
            $('.topnav').removeClass('add-color');
        }
    });
}

// adapted from http://jsfiddle.net/mariusc23/s6mLJ/31/
function useShowOnScrollUp() {
    var didScroll = true;
    var lastScrollTop = 0;
    var delta = 5;
    var navbarHeight = $('.topnav').height();

    $(window).on('scroll', function() {
        didScroll = true;
    })

    setInterval(function() {
        if (didScroll) {
            hasScrolled();
            didScroll = false;
        }
    }, 250);

    function hasScrolled() {
        var st = document.documentElement.scrollTop;

        if(Math.abs(lastScrollTop - st) <= delta)
            return;

        if($('.topnav-collapse').hasClass('show')) 
            return;
        
        if(st > lastScrollTop && st > navbarHeight) {
            $('.topnav').addClass('hide-nav');
        } else {
            // Scroll should not past the document. (It is possible in Mac.)
            var scrollAtTheBottom = document.documentElement.scrollHeight - window.innerHeight;
            if(st < scrollAtTheBottom) {
                $('.topnav').removeClass('hide-nav');
            }
        }

        lastScrollTop = st;
    }
}

useAddColor();
useShowOnScrollUp();