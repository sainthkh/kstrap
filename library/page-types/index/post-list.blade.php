@extends('layouts.default')

@section('content') 
<div class="container" id="content" tabindex="-1">
	<div class="row">
		<div class="col-md-8 offset-md-2 content-area" id="primary">
			<main class="site-main" id="main">
                @wpposts
                    @include('shared.excerpt')
                @elseposts
                    @include('shared.no-content')
				@endposts
			</main><!-- #main -->
			@include('components.pagination')
		</div>
	</div>
</div>
@endsection