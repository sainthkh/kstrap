<header id="masthead" class="site-header hero-area" role="banner">
    <div class="hero-background-image">
        <img src="@mod('hero_background_image')" width="2000" height="1200" alt="Test" />
    </div>
    <div class="black-overlay"></div>
    <div class="hero-message">
        <div class="bg-transparent-black w-100 py-4">
            <div class="container flex-center">
                <div class="flex-center flex-column">
                    <h1 class="headline">@mod('hero_headline')</h1>
                    <h3 class="subhead">@mod('hero_subhead')</h3>
                    <div id="mc_embed_signup" class="col-12 col-md-10 mt-4">
                        @include('components.signup-form')
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('components.layout.navbar')
</header>