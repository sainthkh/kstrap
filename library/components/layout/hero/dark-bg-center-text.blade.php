<header class="header">
    @include('components.layout.navbar')

    <div id="home" class="hero">
        <div class="hero-container">
            <div class="hero-message">
                <h2 class="hero-title">@mod('hero_headline')</h2>
                <hr class="hero-hr">
                <h4 class="hero-subhead">@mod('hero_subhead')</h4>
            </div>
            <a class="hero-cta-btn" href="@mod('hero_cta_link')">
                <i class="fa fa-calendar"></i>
                <span>@mod('hero_cta_text')</span>
            </a>
        </div>
    </div>

</header>
