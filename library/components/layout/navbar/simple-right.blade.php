<div class="navbar-container" id="navbar-container">
	<a class="skip-link screen-reader-text sr-only" href="#content">Skip to content</a>
	<nav class="navbar navbar-expand-md navbar-dark @echoif('bg-dark', !kstrap_front_page())">
		<div class="container">
			@unless(has_custom_logo()) 
				@if(is_front_page() && is_home())
					<h1 class="navbar-brand mb-0">
						<a rel="home" href="{{ home_url( '/' ) }}" title="{{ esc_attr( get_bloginfo( 'name', 'display' ) ) }}"><?php bloginfo( 'name' ); ?></a>
					</h1>
				@else
					<a class="navbar-brand" rel="home" href="{{ home_url( '/' ) }}" title="{{ esc_attr( get_bloginfo( 'name', 'display' ) ) }}"><?php bloginfo( 'name' ); ?></a>
				@endif
			@else 
				the_custom_logo();
			@endunless

			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-nav" aria-controls="main-nav" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			@include('components.nav.main-menu')
		</div><!-- .container -->
	</nav><!-- .site-navigation -->
</div>