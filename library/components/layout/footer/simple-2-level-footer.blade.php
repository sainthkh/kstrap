<footer class="site-footer" id="wrapper-footer">
    <div class="footer-main">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <nav class="social-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Footer Social Links Menu', 'kstrap' ); ?>">
                        @include('components.nav.social')
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <nav class="legal-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Legal menu', 'kstrap' ) ?>">
                        @include('components.nav.legal')
                    </nav>
                </div>
            </div>
            <div class="row mt-3 copyright-text">
                <div class="col-12">
                    @mod('footer_copyright')
                </div>
            </div>
        </div>
    </div>
</footer>